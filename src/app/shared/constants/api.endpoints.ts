interface IAPIEndpoints {
  auth: {
    register: string;
    login: string;
    logout: string;
    resetPwd: string;
  };
}

const apiPrefix = `/api/v1`;

const apiEndpoints: IAPIEndpoints = {
  auth: {
    register: `${apiPrefix}/register`,
    login: `${apiPrefix}/login`,
    logout: `${apiPrefix}/logout`,
    resetPwd: `${apiPrefix}/reset_pwd`
  }
};

export function getAPIEndpoints(): IAPIEndpoints {
  return Object.assign({}, apiEndpoints);
}
