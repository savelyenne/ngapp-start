import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-site-navbar',
  templateUrl: './site-navbar.component.html',
  styleUrls: ['./site-navbar.component.scss']
})
export class SiteNavbarComponent implements OnInit {
  constructor(private router: Router, private authService: AuthService) {
    this.authService.authed().subscribe((r) => {
      this.loggedIn = r;
      this.router.events.pipe(filter((event) => event instanceof NavigationEnd)).subscribe((route: ActivatedRoute) => {
        const url = route.url.toString();
        this.loginViewActive = url.includes('/account/login');
        this.registerViewActive = url.includes('/account/register');
      });
    });
  }

  loggedIn: boolean;
  loginViewActive: boolean;
  registerViewActive: boolean;

  ngOnInit() {}
}
