import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private lsService: LocalStorageService) {}

  getToken(): string {
    return this.lsService.get('token');
  }

  setToken(t: string): boolean {
    return this.lsService.set('token', t);
  }

  authed(): Observable<boolean> {
    return of(!!this.lsService.get('token'));
  }
}
