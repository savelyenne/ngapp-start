import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LocalizedTitleService {

  constructor(private title: Title, private translate: TranslateService) { }

  set(title: string): void {
    this.translate.stream(title).subscribe((translatedTitle: string) => {
      this.title.setTitle(`${translatedTitle} | NG APP`);
    });
  }
}
