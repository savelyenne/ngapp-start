import { TestBed, inject } from '@angular/core/testing';

import { LocalizedTitleService } from './localized-title.service';

describe('LocalizedTitleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalizedTitleService]
    });
  });

  it('should be created', inject([LocalizedTitleService], (service: LocalizedTitleService) => {
    expect(service).toBeTruthy();
  }));
});
