import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { LocalizeRouterModule } from 'localize-router';

@NgModule({
  imports: [CommonModule, TranslateModule, LocalizeRouterModule],
  declarations: [],
  exports: [CommonModule, TranslateModule, LocalizeRouterModule]
})
export class LocalizationModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: LocalizationModule
    };
  }
}
