import { LocalizationModule } from './localization.module';

describe('LocalizationModule', () => {
  let localizationModule: LocalizationModule;

  beforeEach(() => {
    localizationModule = new LocalizationModule();
  });

  it('should create an instance', () => {
    expect(localizationModule).toBeTruthy();
  });
});
