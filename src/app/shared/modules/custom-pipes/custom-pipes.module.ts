import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormFieldValidationErrorsPipe } from '../../pipes/form-field-validation-errors.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [FormFieldValidationErrorsPipe],
  exports: [FormFieldValidationErrorsPipe]
})
export class CustomPipesModule {}
