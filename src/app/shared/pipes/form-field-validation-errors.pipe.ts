import { Pipe, PipeTransform } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

import { _ } from '../helpers/translate';

@Pipe({
  name: 'localizedError'
})
export class FormFieldValidationErrorsPipe implements PipeTransform {
  constructor(private translate: TranslateService) {}

  transform(value: ValidationErrors | null, args?: any): Observable<string | Object> {
    // { "required": true }
    // { "email": true }
    const localizedErrors = {
      required: _('Please fill in the required field'),
      email: _('Please enter valid email address')
    };

    if (value) {
      const error = localizedErrors[Object.keys(value)[0]];

      if (error) {
        return this.translate.stream(error);
      }
    }

    return null;
  }
}
