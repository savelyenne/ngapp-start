import { Location } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LocalStorageModule } from 'angular-2-local-storage';
import { LocalizeParser, LocalizeRouterModule, LocalizeRouterSettings } from 'localize-router';
import { LocalizeRouterHttpLoader } from 'localize-router-http-loader';

import { AppRoutingModule, routes } from './app-routing.module';
import { AppComponent } from './app.component';
import { LocalizationModule } from './shared/modules/localization/localization.module';
import { UIKitModule } from './shared/modules/ui-kit/ui-kit.module';
import { SiteNavbarComponent } from './shared/components/site-navbar/site-navbar.component';
import { httpInterceptorProviders } from './shared/http-interceptors';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [AppComponent, SiteNavbarComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    UIKitModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    LocalizeRouterModule.forRoot(routes, {
      parser: {
        provide: LocalizeParser,
        useFactory: (translate, location, settings, http) =>
          new LocalizeRouterHttpLoader(translate, location, settings, http),
        deps: [TranslateService, Location, LocalizeRouterSettings, HttpClient]
      },
      alwaysSetPrefix: false
    }),
    LocalizationModule.forRoot(),
    ReactiveFormsModule,
    LocalStorageModule.withConfig({
      prefix: 'ngapp',
      storageType: 'localStorage'
    })
  ],
  providers: [Title, httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(translate: TranslateService) {
    // NB lang to use as a fallback when there's no translation in current language
    translate.setDefaultLang('en');
  }
}
