import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomPipesModule } from '../../shared/modules/custom-pipes/custom-pipes.module';
import { LocalizationModule } from '../../shared/modules/localization/localization.module';
import { UIKitModule } from '../../shared/modules/ui-kit/ui-kit.module';
import { LoginRoutingModule } from './account-routing.module';
import { ForgotPwdComponent } from './components/forgot-pwd/forgot-pwd.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserAccountService } from './services/user-account.service';

@NgModule({
  imports: [CommonModule, LoginRoutingModule, UIKitModule, LocalizationModule, ReactiveFormsModule, CustomPipesModule],
  declarations: [LoginComponent, RegisterComponent, ForgotPwdComponent],
  providers: [UserAccountService]
})
export class AccountModule {}
