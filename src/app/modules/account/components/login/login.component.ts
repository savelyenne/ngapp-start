import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { LocalizedTitleService } from '../../../../shared/services/localized-title.service';
import { UserAccountService } from '../../services/user-account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private uaService: UserAccountService, private localizedTitle: LocalizedTitleService) {}

  loginForm: FormGroup;
  hidePwd = true;

  submit() {
    if (this.loginForm.valid) {
      this.uaService.login(this.loginForm.value).subscribe();
    } else {
      for (const control of Object.keys(this.loginForm.controls)) {
        this.loginForm.controls[control].markAsTouched();
      }
    }
  }

  ngOnInit() {
    this.localizedTitle.set('Log into your account');
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required)
    });
  }
}
