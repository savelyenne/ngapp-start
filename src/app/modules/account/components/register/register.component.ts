import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { LocalizedTitleService } from '../../../../shared/services/localized-title.service';
import { UserAccountService } from '../../services/user-account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  constructor(private uaService: UserAccountService, private localizedTitle: LocalizedTitleService) {}

  registerForm: FormGroup;
  hidePwd = true;

  submit() {
    if (this.registerForm.valid) {
      this.uaService.register(this.registerForm.value).subscribe();
    } else {
      for (const control of Object.keys(this.registerForm.controls)) {
        this.registerForm.controls[control].markAsTouched();
      }
    }
  }

  ngOnInit() {
    this.localizedTitle.set('Register an account');
    this.registerForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required)
    });
  }
}
