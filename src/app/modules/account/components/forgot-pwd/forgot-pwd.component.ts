import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { LocalizedTitleService } from '../../../../shared/services/localized-title.service';
import { UserAccountService } from '../../services/user-account.service';

@Component({
  selector: 'app-forgot-pwd',
  templateUrl: './forgot-pwd.component.html',
  styleUrls: ['./forgot-pwd.component.scss']
})
export class ForgotPwdComponent implements OnInit {
  constructor(private uaService: UserAccountService, private localizedTitle: LocalizedTitleService) {}

  forgotPwdForm: FormGroup;

  submit() {
    if (this.forgotPwdForm.valid) {
      this.uaService.resetPwd(this.forgotPwdForm.value).subscribe();
    } else {
      this.forgotPwdForm.controls.email.markAsTouched();
    }
    alert('Not implemented yet :(');
  }

  ngOnInit() {
    this.localizedTitle.set('Reset your account password');
    this.forgotPwdForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }
}
