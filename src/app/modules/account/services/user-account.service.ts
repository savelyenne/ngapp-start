import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { getAPIEndpoints } from '../../../shared/constants/api.endpoints';
import { AuthService } from '../../../shared/services/auth.service';
import { IAccount } from '../interfaces/account';
import { ILoginCredentials } from '../interfaces/login-credentials';

@Injectable()
export class UserAccountService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  private urls = Object.assign({}, getAPIEndpoints().auth);

  register(acc: IAccount) {
    return this.http.put(this.urls.register, acc).pipe(
      map((r) => {
        this.authService.setToken(r['token']);
      })
    );
  }

  login(creds: ILoginCredentials) {
    return this.http.post(this.urls.login, creds).pipe(
      map((r) => {
        this.authService.setToken(r['token']);
      })
    );
  }

  logout() {
    return this.http.post(this.urls.logout, {}).pipe(
      map((r) => {
        this.authService.setToken(r['token']);
      })
    );
  }

  resetPwd(email: string) {
    return this.http.post(this.urls.resetPwd, { email });
  }
}
