import { Component, OnInit } from '@angular/core';

import { _ } from './shared/helpers/translate';
import { LocalizedTitleService } from './shared/services/localized-title.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(private localizedTitle: LocalizedTitleService) {}

  ngOnInit() {
    _('Home');
    this.localizedTitle.set('Home');
  }
}
